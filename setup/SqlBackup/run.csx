#r "Microsoft.WindowsAzure.Storage"
#r "Newtonsoft.Json"

using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Azure.Batch;
using Microsoft.Azure.Batch.Auth;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using ProWorks.Azure.Batch;
using Newtonsoft.Json;

public static void Run(TimerInfo myTimer, ILogger log)
{
    log.LogInformation($"Timer triggered at: {DateTime.Now}");

    var blobContainer = Environment.GetEnvironmentVariable("DbBlobContainer");
    if (string.IsNullOrWhiteSpace(blobContainer)) blobContainer = "backups";

    var backupPrefix = Environment.GetEnvironmentVariable("BackupFilePrefix");
    if (string.IsNullOrWhiteSpace(backupPrefix)) backupPrefix = string.Empty;
    else if (!backupPrefix.EndsWith("-")) backupPrefix += "-";

    var dBConnStrings = Environment.GetEnvironmentVariable("DbConnStrings");
    if (string.IsNullOrWhiteSpace(dBConnStrings)) throw new ArgumentNullException("DbConnStrings");

    var storageConnectionString = Environment.GetEnvironmentVariable("AzureWebJobsStorage");
    var dBList = JsonConvert.DeserializeObject<List<DB>>(dBConnStrings);

    foreach (var dB in dBList)
    {
        var dbName = dB.DbName;
        var dbServer = dB.DbServer;
        var dbUsername = dB.DbUserName;
        var dbPassword = DecryptPassword(dB.DbPassword);

        var currentDateTime = DateTime.Now.ToUniversalTime().ToString("s").Replace(':', '-');

        var name = $"{backupPrefix}{dbName}-{currentDateTime}.bacpac";
        var dbConnectionString = $"Data Source={dbServer};Initial Catalog={dbName};User ID={dbUsername};Password={dbPassword}";
        var dbMaskedConnString = $"Data Source={dbServer};Initial Catalog={dbName};User ID={dbUsername};Password=********";
        log.LogInformation($"Starting bacpac generation from '{dbMaskedConnString}' to '{name}'");

        try
        {
            log.LogInformation("Creating the batch helper");
            var config = new Dictionary<string, string>();
            config[AzureBatchHelper.ConfigKeyBatchAccountKey] = Environment.GetEnvironmentVariable("BatchAccountKey");
            config[AzureBatchHelper.ConfigKeyBatchAccountName] = Environment.GetEnvironmentVariable("BatchAccountName");
            config[AzureBatchHelper.ConfigKeyBatchAccountUrl] = Environment.GetEnvironmentVariable("BatchAccountUrl");
            config[AzureBatchHelper.ConfigKeyAzureWebJobsStorage] = storageConnectionString;
            var helper = AzureBatchHelper.CreateFromConfig(config);
            helper.InformationLogger = (m) => log.LogInformation(m);
            helper.ErrorLogger = (m) => log.LogError(m);

            PerformExport(log, helper, dbServer, dbConnectionString, storageConnectionString, dbName, blobContainer, name, TimeSpan.FromDays(1));
        }
        catch (Exception e)
        {
            log.LogError("Could not export the bacpac file", e);
            throw;
        }

        log.LogInformation($"Successfully exported the BacPac file to '{name}'");
    }
}

private static async Task PerformExport(ILogger log, AzureBatchHelper helper, string dbServer, string sqlConnectionString, string storageConnectionString, string databaseName, string containerName, string bacpacFilePath, TimeSpan waitTimeOut)
{
    try
    {
        log.LogInformation("Starting the SQL export");
        log.LogInformation($"    dbServer:       {dbServer}");
        log.LogInformation($"    dbName:         {databaseName}");
        log.LogInformation($"    blobContainer:  {containerName}");
        log.LogInformation($"    bacpacFilePath: {bacpacFilePath}");
        await helper.RunSqlExport(sqlConnectionString, storageConnectionString, databaseName, containerName, bacpacFilePath, waitTimeOut);

        log.LogInformation($"Successfully exported the BacPac file to '{bacpacFilePath}'");
    }
    catch (Exception e)
    {
        log.LogError("Could not export the database", e);
    }
    finally
    {
        try
        {
            // Delete the job and pool so we aren't getting billed for them running
            var batchAccountKey = Environment.GetEnvironmentVariable("ProWorks.Azure.Batch:BatchAccountKey");
            using (var client = BatchClient.Open(new BatchSharedKeyCredentials(helper.BatchAccountUrl, helper.BatchAccountName, batchAccountKey)))
            {
                var pool = client.PoolOperations.ListPools().FirstOrDefault(p => p.Id == "SqlJobPool");
                if (pool != null)
                {
                    var jobs = client.JobOperations.ListJobs().Where(j => j.PoolInformation.PoolId == pool.Id);

                    foreach (var job in jobs)
                    {
                        log.LogInformation($"Deleting the job {job.Id}");
                        await client.JobOperations.DeleteJobAsync(job.Id);
                    }

                    log.LogInformation($"Deleting the pool {pool.Id}");
                    await client.PoolOperations.DeletePoolAsync(pool.Id);
                }
            }
        }
        catch (Exception e)
        {
            log.LogError("Could not delete the jobs and pool", e);
        }
    }
}

private static readonly System.Text.RegularExpressions.Regex AppSettingPattern = new System.Text.RegularExpressions.Regex("\\@\\{application-setting:(?<key>[^}]+)\\}");
private static string DecryptPassword(string password)
{
    if (string.IsNullOrEmpty(password)) return password;

    password = AppSettingPattern.Replace(password, m => {
		var keyName = m.Groups["key"].Value;
		var envVal = Environment.GetEnvironmentVariable(keyName);
		return envVal;
    });

    return password;
}

public class DB
{
    public string DbName { get; set; }
    public string DbServer { get; set; }
    public string DbUserName { get; set; }
    public string DbPassword { get; set; }
}
