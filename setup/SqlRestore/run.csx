#r "Microsoft.WindowsAzure.Storage"
#r "Newtonsoft.Json"

using System.Net;
using Microsoft.Azure.Batch;
using Microsoft.Azure.Batch.Auth;
using Microsoft.WindowsAzure.Storage;
using ProWorks.Azure.Batch;
using Newtonsoft.Json;

public static void Run(TimerInfo myTimer, ILogger log)
{
    log.LogInformation($"Timer triggered at: {DateTime.Now}");

    var blobContainer = Environment.GetEnvironmentVariable("DbBlobContainer");
    if (string.IsNullOrWhiteSpace(blobContainer)) blobContainer = "backups";

    var backupPrefix = Environment.GetEnvironmentVariable("BackupFilePrefix");
    if (string.IsNullOrWhiteSpace(backupPrefix)) backupPrefix = string.Empty;
    else if (!backupPrefix.EndsWith("-")) backupPrefix += "-";

    var dBConnStrings = Environment.GetEnvironmentVariable("DbConnStrings");
    if (string.IsNullOrWhiteSpace(dBConnStrings)) throw new ArgumentNullException("DbConnStrings");

    var dBList = JsonConvert.DeserializeObject<List<DB>>(dBConnStrings);

    foreach (var dB in dBList)
    {
		if (dB.Dest == null) continue;
		
        var srcDbName = dB.DbName;
        var dbName = dB.Dest.DbName;
        var dbServer = dB.Dest.DbServer;
        var dbUsername = dB.Dest.DbUserName;
        var dbPassword = DecryptPassword(dB.Dest.DbPassword);
        var cacheUrl = dB.Dest.UmbracoCacheUrl;

        var storageConnectionString = Environment.GetEnvironmentVariable("AzureWebJobsStorage");
		var bacpacAccount = CloudStorageAccount.Parse(storageConnectionString);
		var bacpacStorageClient = bacpacAccount.CreateCloudBlobClient();
		var currentBlobs = bacpacStorageClient.ListBlobs($"{blobContainer}/{backupPrefix}{srcDbName}-");
		var recentBackupTime = DateTime.Now.AddDays(-1.0).ToUniversalTime().ToString("s").Replace(':', '-');
		string name = null;

		foreach (var currentBlob in currentBlobs)
		{
			var blobName = currentBlob.Uri.Segments[currentBlob.Uri.Segments.Length - 1];
			blobName = blobName.Substring(backupPrefix.Length + srcDbName.Length + 1);
			blobName = blobName.Substring(0, blobName.Length - ".bacpac".Length);
			if (blobName.CompareTo(recentBackupTime) <= 0) continue;

			name = currentBlob.Uri.Segments[currentBlob.Uri.Segments.Length - 1];
			break;
		}

		if (name == null)
		{
			log.LogInformation($"Could not find a backup of {srcDbName} more recent than {recentBackupTime} to restore to {dbName}");
			continue;
		}

        var dbConnectionString = $"Data Source={dbServer};User ID={dbUsername};Password={dbPassword}";
        var dbMaskedConnString = $"Data Source={dbServer};User ID={dbUsername};Password=********";
        log.LogInformation($"Starting bacpac restore from '{name}' to '{dbMaskedConnString}'");

        try
        {
            log.LogInformation("Creating the batch helper");
            var config = new Dictionary<string, string>();
            config[AzureBatchHelper.ConfigKeyBatchAccountKey] = Environment.GetEnvironmentVariable("BatchAccountKey");
            config[AzureBatchHelper.ConfigKeyBatchAccountName] = Environment.GetEnvironmentVariable("BatchAccountName");
            config[AzureBatchHelper.ConfigKeyBatchAccountUrl] = Environment.GetEnvironmentVariable("BatchAccountUrl");
            config[AzureBatchHelper.ConfigKeyAzureWebJobsStorage] = storageConnectionString;
            var helper = AzureBatchHelper.CreateFromConfig(config);
            helper.InformationLogger = (m) => log.LogInformation(m);
            helper.ErrorLogger = (m) => log.LogError(m);

            PerformImport(log, helper, dbServer, dbConnectionString, storageConnectionString, dbName, blobContainer, name, cacheUrl, TimeSpan.FromDays(1));
        }
        catch (Exception e)
        {
            log.LogError("Could not import the bacpac file", e);
            throw;
        }
    }
}

private static async Task PerformImport(ILogger log, AzureBatchHelper helper, string dbServer, string sqlConnectionString, string storageConnectionString, string databaseName, string containerName, string bacpacFilePath, string cacheUrl, TimeSpan waitTimeOut)
{
    try
    {
        log.LogInformation("Starting the SQL import");
        log.LogInformation($"    dbServer:       {dbServer}");
        log.LogInformation($"    dbName:         {databaseName}");
        log.LogInformation($"    blobContainer:  {containerName}");
        log.LogInformation($"    bacpacFilePath: {bacpacFilePath}");
        await helper.RunSqlImport(sqlConnectionString, storageConnectionString, databaseName, containerName, bacpacFilePath, waitTimeOut);

        log.LogInformation($"Successfully imported the BacPac file from '{bacpacFilePath}' to '{dbServer}.{databaseName}'");
    }
    catch (Exception e)
    {
        log.LogError("Could not import the bacpac file", e);
    }
    finally
    {
        try
        {
            // Trigger a refresh in the Umbraco back-office
            if (!string.IsNullOrWhiteSpace(cacheUrl))
            {
                using (var client = new WebClient())
                {
                    log.LogInformation($"Attempting to refresh the Umbraco cache using {cacheUrl}");
                    var result = await client.DownloadStringTaskAsync(cacheUrl);
                }
            }
        }
        catch (Exception e)
        {
            log.LogError("Could not update the Umbraco cache file", e);
        }
        finally
        {
            try
            {
                // Delete the job and pool so we aren't getting billed for them running
                var batchAccountKey = Environment.GetEnvironmentVariable("ProWorks.Azure.Batch:BatchAccountKey");
                using (var client = BatchClient.Open(new BatchSharedKeyCredentials(helper.BatchAccountUrl, helper.BatchAccountName, batchAccountKey)))
                {
                    var pool = client.PoolOperations.ListPools().FirstOrDefault(p => p.Id == "SqlJobPool");
                    if (pool != null)
                    {
                        var jobs = client.JobOperations.ListJobs().Where(j => j.PoolInformation.PoolId == pool.Id);

                        foreach (var job in jobs)
                        {
                            log.LogInformation($"Deleting the job {job.Id}");
                            await client.JobOperations.DeleteJobAsync(job.Id);
                        }

                        log.LogInformation($"Deleting the pool {pool.Id}");
                        await client.PoolOperations.DeletePoolAsync(pool.Id);
                    }
                }
            }
            catch (Exception e)
            {
                log.LogError("Could not delete the jobs and pool", e);
            }
        }
    }
}

private static readonly System.Text.RegularExpressions.Regex AppSettingPattern = new System.Text.RegularExpressions.Regex("\\@\\{application-setting:(?<key>[^}]+)\\}");
private static string DecryptPassword(string password)
{
    if (string.IsNullOrEmpty(password)) return password;

    password = AppSettingPattern.Replace(password, m => {
		var keyName = m.Groups["key"].Value;
		var envVal = Environment.GetEnvironmentVariable(keyName);
		return envVal;
    });

    return password;
}

public class DB
{
    public string DbName { get; set; }
    public string DbServer { get; set; }
    public string DbUserName { get; set; }
    public string DbPassword { get; set; }
    public string UmbracoCacheUrl { get; set; }
	public DB Dest { get; set; }
}