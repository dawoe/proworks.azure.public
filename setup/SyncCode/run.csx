#r "Newtonsoft.Json"

using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

public static IActionResult Run(HttpRequest req, ILogger log, ExecutionContext context)
{
    try
    {
        log.LogInformation($"Received a {req?.Method} request");

        var reposStr = Environment.GetEnvironmentVariable("CodeRepositories");
        Repo dtl = null;
        var found = false;

        if (req != null && req.ContentLength > 0)
        {
            using (var rdr = new StreamReader(req.Body))
            {
                var body = rdr.ReadToEnd();
                log.LogInformation($"Request Body:\r\n{body}");

                dtl = JsonConvert.DeserializeObject<Repo>(body);
                if (!found && dtl?.url != null) { log.LogInformation("Found repository in POST body"); found = true; }
            }
        }

        if (dtl?.url == null && req.Query.Keys.Contains("siteName") && req.Query.Keys.Contains("hostName"))
            dtl = new Repo {siteName = req.Query["siteName"][0], hostName = req.Query["hostName"][0]};
        if (!found && dtl?.url != null) { log.LogInformation("Found repository in query siteName and hostName"); found = true; }
        if (dtl?.url == null && req.Query.Keys.Contains("srcUrl")) dtl = new Repo { url = req.Query["srcUrl"][0] };
        if (!found && dtl?.url != null) { log.LogInformation("Found repository in query url"); found = true; }

        var repos = JsonConvert.DeserializeObject<List<SrcRepo>>(reposStr ?? "null");
        if (repos == null || repos.Count == 0) throw new ArgumentException("You must specify one or more repositories in the CodeRepositories application setting");

        var src = string.IsNullOrWhiteSpace(dtl?.url) && repos.Count == 1
            ? repos[0]
            : (dtl?.url != null ? repos.FirstOrDefault(r => r?.siteName == dtl.siteName && r?.hostName == dtl.hostName) : null);
        if (!found && src?.url != null) { log.LogInformation("Found repository as only defined repository"); found = true; }

        if (src?.url == null && dtl?.url != null) throw new ArgumentException($"Could not find a valid repository definition to synchronize for url {dtl.url} (hostName: {dtl.hostName}, siteName: {dtl.siteName})");
        else if (src?.url == null) throw new ArgumentException("Could not find a valid repository definition to synchronize");
        if (src.dest?.url == null) throw new ArgumentException("The specified repository does not have a valid destination to synchronize with");

        var repoName = string.IsNullOrWhiteSpace(src.name) ? Regex.Replace(src.siteName, "[^A-Za-z0-9_-]", "") : src.name;

        // Intentionally calling this without awaiting so that the web request returns quickly,
        // and the actual sync happens in the background
        SyncRepository(repoName, src, log);

        return new OkObjectResult("Successfully initiated repository synchronization");
    }
    catch (Exception ex)
    {
        log.LogError("Could not synchronize the repositories - " + ex);
        return new BadRequestObjectResult("Could not synchronize the repositories.  Review the logs for any error messages.");
    }
}

public static async Task SyncRepository(string repoName, SrcRepo src, ILogger log)
{
    try
    {
        var baseDir = @"D:\home";
        var repoDir = $"{baseDir}\\{repoName}";
        var success = true;

        log.LogInformation($"Using a repository directory of {repoDir}");

        if (!Directory.Exists(repoDir)) success = await ExecuteGit(baseDir, $"clone -v --mirror \"{src.url}\" \"{repoName}\"", success, log);
        else success = await ExecuteGit(repoDir, "fetch origin", success, log);

        success = await ExecuteGit(repoDir, $"push -v -f \"{src.dest.url}\"", success, log);
    }
    finally
    {
        log.LogInformation("Completed repository sync process");
    }
}

public static Task<bool> ExecuteGit(string workingDir, string arguments, bool success, ILogger log)
{
    var tcs = new TaskCompletionSource<bool>();
	if (!success) { tcs.SetResult(false); return tcs.Task; }

    try
    {
        var proc = new Process();

        proc.StartInfo.UseShellExecute = false;
        proc.StartInfo.FileName = @"D:\Program Files\Git\cmd\git.exe";
        proc.StartInfo.Arguments = arguments;
        proc.StartInfo.CreateNoWindow = true;
        proc.StartInfo.WorkingDirectory = workingDir;
        proc.StartInfo.RedirectStandardError = true;
        proc.StartInfo.RedirectStandardOutput = true;
        proc.ErrorDataReceived += (s,e) => log.LogError(SanitizeOutput(e.Data));
        proc.OutputDataReceived += (s,e) => log.LogInformation(SanitizeOutput(e.Data));
        proc.Exited += (s,e) => { tcs.SetResult(proc.ExitCode == 0); proc.Dispose(); };
        proc.EnableRaisingEvents = true;

        var logArgs = SanitizeOutput(arguments);
        log.LogInformation($"Executing Git command '{logArgs}'");
        proc.Start();
        proc.BeginErrorReadLine();
        proc.BeginOutputReadLine();
    }
    catch (Exception ex)
    {
        log.LogError("Could not execute the Git command - " + ex);
        tcs.SetResult(false);
    }

    return tcs.Task;
}

private static string SanitizeOutput(string message)
{
    return message != null ? Regex.Replace(message, "(/[^: /]+:)[^@/]+@", "$1********@") : string.Empty;
}

public class Repo
{
	private string _url;

	public string siteName { get; set; }
	public string hostName { get; set; }
	public string userName { get; set; }
	public string password { get; set; }
    public ILogger log { get; set; }

	public string url
	{
		get
		{
			if (!string.IsNullOrWhiteSpace(_url)) return _url;
			if (string.IsNullOrWhiteSpace(hostName) || string.IsNullOrWhiteSpace(siteName)) return null;

			var builder = new UriBuilder($"https://{hostName}/{siteName}.git");

			if (!string.IsNullOrWhiteSpace(userName)) builder.UserName = Uri.EscapeDataString(userName);
			if (!string.IsNullOrWhiteSpace(password)) builder.Password = Uri.EscapeDataString(DecryptPassword(password));

			_url = builder.ToString();

			return _url;
		}
		set
		{
			var uri = string.IsNullOrWhiteSpace(value) ? null : new Uri(value);
			_url = null;
			siteName = null;
			hostName = null;
			userName = null;
			password = null;
			if (uri == null) return;

			hostName = uri.Host;
			if (uri.Segments.Length > 1)
			{
				siteName = uri.Segments[uri.Segments.Length - 1];
				if (siteName.EndsWith(".git")) siteName = siteName.Substring(0, siteName.Length - 4);
			}
			if (!string.IsNullOrWhiteSpace(uri.UserInfo))
			{
				var splitIdx = uri.UserInfo.IndexOf(":");
				userName = splitIdx > 0 ? uri.UserInfo.Substring(0, splitIdx) : uri.UserInfo;
				userName = Uri.UnescapeDataString(userName);

				password = splitIdx > 0 && (splitIdx + 1) < uri.UserInfo.Length ? uri.UserInfo.Substring(splitIdx + 1) : string.Empty;
				password = Uri.UnescapeDataString(password);
			}
		}
	}

    private static readonly System.Text.RegularExpressions.Regex AppSettingPattern = new System.Text.RegularExpressions.Regex("\\@\\{application-setting:(?<key>[^}]+)\\}");
    private static string DecryptPassword(string password)
    {
        if (string.IsNullOrEmpty(password)) return password;

        password = AppSettingPattern.Replace(password, m => {
            var keyName = m.Groups["key"].Value;
            var envVal = Environment.GetEnvironmentVariable(keyName);
            return envVal;
        });

        return password;
    }
}

public class SrcRepo : Repo
{
	public string name { get; set; }
	public Repo dest { get; set; }
}