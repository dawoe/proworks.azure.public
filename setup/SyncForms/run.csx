#r "Microsoft.WindowsAzure.Storage"

using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

public static async Task Run(Stream myBlob, string name, ILogger log)
{
    var container = "form-data";
    var destinationStorageConnectionString = Environment.GetEnvironmentVariable("destinationStorageConnectionString");

    log.LogInformation($"Blob created\n Container: {container}\n Name: {name}\n Size: {myBlob.Length} Bytes");

    // Set up destination storage account creation
    CloudStorageAccount destinationAccount = CloudStorageAccount.Parse(destinationStorageConnectionString);
    CloudBlobClient destinationStorageClient = destinationAccount.CreateCloudBlobClient();

    // Create the replica container if it doesn't exist
    var destinationContainer = destinationStorageClient.GetContainerReference(container);
    try
    {
        await destinationContainer.CreateIfNotExistsAsync();
    }
    catch (Exception e)
    {
        log.LogError(e.Message);
    }

    CloudBlockBlob destinationBlob = destinationContainer.GetBlockBlobReference(name);
    // Create the replica
    log.LogInformation($"Copying {name} to {destinationBlob.Uri.ToString()}");
    try
    {
        await destinationBlob.UploadFromStreamAsync(myBlob);
        log.LogInformation($"Successfully copied {name}");
    }
    catch (Exception e)
    {
        log.LogError(e.Message);
    }
}
