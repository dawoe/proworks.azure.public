# Overview
Broadly speaking, there are a number of Azure components that need to be setup, and a number of changes to the project files, that both need to be done before you can setup an Azure failover site.  For the most part, you can complete the Azure setup and the project setup in any order.  The exceptions would be the Azure Function setup requires all the other Azure setup pieces to be done first, and the forms and media synchronization requires the Azure Function setup to be completed first.  Once you have all the setup pieces completed, you can then setup the failover website, and configure any failover processes.

# Azure Setup

## Create Storage Accounts
1. Create a storage account in the West Europe region, and one in the local region
2. Create a blob container in both accounts called form-data
3. Create a blob container in both accounts called media
4. For the local region account, view the Access Keys area of the account details and grab the connection string, as this will be needed later in the Azure Function setup
5. Create a blob container in the West Europe region called backups, which will be used for SQL backups

## Database Setup
1. Create a database server in the local region
2. Decide on a database name for each database you are going to want to restore, although you do not need to create any databases at this time
3. Note the database server hostname, the admin user name and password, and the database names as you will need them later when setting up the Azure Function

## Batch setup
1. ZIP up the ProWorks.Azure.Sql folder into a ProWorks.Azure.Sql.zip file
2. Create a batch account in the West Europe region, selecting the West Europe storage account as the associated storage account (use the Select a Storage Account link on the batch account creation page)
3. View the Keys and take note of the name, URL, and key for use later
4. Click on the Applications node on the batch account detail
5. Click +Add at the top, and fill out the information as follows:
    - Application Id: ProWorks_Azure_Sql
    - Version: 1.0.0
    - File: Browse and find the ProWorks.Azure.Sql.zip folder
6. Click on the ProWorks_Azure_Sql application, then browse to the Settings node
7. Set the default version of the application to be 1.0.0, and click Save

## Vault setup
1. Create a new Key Vault in the West Europe region
2. Setup Secrets for any password or key you want protected, such as the following
    1. The password of the account you use to access the Umbraco Cloud
    2. The admin password for the database server
    3. The connection string for the destination storage account
    4. The key for your batch account

## Azure Function Setup
1. Create a new Azure Function App, in your existing West Europe storage account
2. Setup Vault access for the function
    1. On the Platform Features, choose Identity and turn on the system identity
    2. In your Key Vault, go to the Access Policies section
    3. Create a new policy
        - Template - leave blank
        - Principal - Search for the name of your Function App
        - Key Permissions - None selected
        - Secret Permissions - Get
        - Certificate Permissions - None selected
        - Authorized Application - None selected
3. On the Platform Features, click Application Settings, and setup using the instructions below and the template file sync-umbraco.applicationSettings.txt as an example
    1. BackupFilePrefix - Any prefix you want for the backed-up files
    2. BatchAccountKey - A reference to the batch account key secret in your Vault account
    3. BatchAccountName - The name of your batch account
    4. BatchAccountUrl - The URL of your batch account
    5. CodeRepositories - A JSON array of the code repositories that will be synchronized by the SyncCode function.  Note that you will not know the destination code repositories until after you create the WebApp below.  Once you create the WebApp, you'll need to come back and update this property
    6. DbBlobContainer - The name of the blob container that will contain the DB backups
    7. DbConnStrings - A JSON array of the databases that will be backed up each night, and if desired, restored to your failover server
        - The information for each source database should be retrieved using the Umbraco Cloud dashboard, in the Manage Connections area
        - The destination databases, if desired, are from your failover DB setup above
    8. DestinationStorageConnectionString - A reference to the secret in your Vault account that contains the connection string for the storage account that forms and media will be synchronized with
    9. FailoverDbAdminPassword - A reference to the secret in your Vault account that contains the password for the admin user on the fail-over DB
    10. UmbracoCloudDbAdminPassword - A reference to the secret in your Vault account that contains the password for the access user on the Umbraco Cloud database
    10. UmbracoCloudUserPassword - A reference to the secret in your Vault account that contains the password for your Umbraco Cloud user

    `NOTE: The CodeRepositories and DbConnStrings can embed references to other application settings for secure passwords.  See the CodeRepositories example for the syntax.  Any application setting can reference a vault item, and it will be loaded automatically at run-time.  If you want to keep your storage key secret, or any other sensitive data secret, you can use this combination of custom application settings, as well as embedded password references, to protect private data.`

4. Add 5 functions to the function app, using the files in the attached folders, and the instructions below
    1. SqlBackup - A timer triggered function, with an interval of "0 0 7 * * *"
    2. SqlRestore - A timer triggered function, with an interval of "0 0 10 * * *"
    3. SyncCode - An HTTP triggered function, with function authentication
    4. SyncForms - An Azure Blob Storage triggered function, with a path of "form-data/{name}"
    5. SyncMedia - An Azure Blob Storage triggered function, with a path of "media/{name}"
5. Test the SqlBackup function to generate a backup of the current databases
6. Test the SqlRestore function to create the restored databases

## Synchronize Forms and Media
1. Using the Azure Storage Explorer (http://storageexplorer.com), upload the following to the West Europe storage account (test with one file each first and see step 2):
    - The contents of your /App_Data/UmbracoForms/Data folder to the form-data container
    - The contents of your /media folder to the media container
2. Verify that the files copied over successfully to your local region storage account

# Project Setup

## Add the Examine config for Azure Web Apps
Modify the config/ExamineSettings.config file, add the following to all the index providers:

`directoryFactory="Examine.LuceneEngine.Directories.TempEnvDirectoryFactory,Examine"`

## Setup media to use Blob storage
1. Install the ImageProcessor.Web.Config package from NuGet
2. Install the UmbracoFileSystemProviders.Azure package from NuGet
3. Edit the config/FileSystemProviders.config file, setting the media and forms providers as follows:
```xml
<Provider alias="media" type="Our.Umbraco.FileSystemProviders.Azure.AzureBlobFileSystem, Our.Umbraco.FileSystemProviders.Azure">
    <Parameters>
        <add key="alias" value="media"/>
    </Parameters>
</Provider>
<Provider alias="forms" type="Our.Umbraco.FileSystemProviders.Azure.AzureBlobFileSystem, Our.Umbraco.FileSystemProviders.Azure">
    <Parameters>
        <add key="alias" value="forms"/>
    </Parameters>
</Provider>
```
4. Add the following appSettings to the web.config file, using the correct values from setting up the storage account above
```xml
<add key="AzureBlobFileSystem.ConnectionString:media" value="DefaultEndpointsProtocol=https;AccountName=[myAccountName];AccountKey=[myAccountKey];EndpointSuffix=core.windows.net" />
<add key="AzureBlobFileSystem.ContainerName:media" value="media" />
<add key="AzureBlobFileSystem.RootUrl:media" value="http://[myAccountName].blob.core.windows.net/" />
<add key="AzureBlobFileSystem.MaxDays:media" value="365" />
<add key="AzureBlobFileSystem.UseDefaultRoute:media" value="true" />
<add key="AzureBlobFileSystem.UsePrivateContainer:media" value="false" />
<add key="AzureBlobFileSystem.ConnectionString:forms" value="DefaultEndpointsProtocol=https;AccountName=[myAccountName];AccountKey=[myAccountKey];EndpointSuffix=core.windows.net" />
<add key="AzureBlobFileSystem.ContainerName:forms" value="form-data" />
<add key="AzureBlobFileSystem.RootUrl:forms" value="http://[myAccountName].blob.core.windows.net/" />
<add key="AzureBlobFileSystem.MaxDays:forms" value="365" />
<add key="AzureBlobFileSystem.UseDefaultRoute:forms" value="true" />
<add key="AzureBlobFileSystem.UsePrivateContainer:forms" value="false" />
```

# Setup an Azure WebApp
`NOTE: If you want to use Traffic Manager, you must use a WebApp tier that supports it.  See the list of supported features below each tier as you are choosing the performance tier of your WebApp.  Additionally, if your site is larger than 1 GB in size, you will need to use a tier that supports the size of the site`

1. Create a new WebApp
2. Setup the database connection string umbracoDbDSN to point to the new Azure database
3. Set the following application settings, using similar values as used above, but pointing to the local region storage accounts instead of the West Europe ones
    - `AzureBlobFileSystem.ConnectionString:media`
    - `AzureBlobFileSystem.RootUrl:media`
    - `AzureBlobFileSystem.ConnectionString:forms`
    - `AzureBlobFileSystem.RootUrl:forms`
4. Use the Deployment Center to add an Local Git Deployment, using the Kudu build server
5. Grab the Git URL and update the CodeRepositories application setting on your Function App with this value
6. Use Kudu on the WebApp to upload the contents of the `tools` folder in this directory to `D:\home\site\deployments\tools` on the server, overwriting the existing deploy.cmd file
7. Test the SyncCode function in the Function App to initially synchronize the repository and perform the first code deploy

# Failover Steps
If you do not setup the High Availability configuration below, which requires an additional investment in Azure, you will need to manually fail-over the site when Umbraco Cloud goes down.  The longest component in the fail-over process will likely be the time for DNS changes to propogate.  If you want to minimize this time, you can set the TTL value in DNS on your production site URL to something smaller, such as a few minutes, rather than the normal default of hours or even days.  Below are the manual steps you would need to carry out to fail-over the site.

1. Switch the DNS to point to the new Azure WebApp site
2. Optionally step up the performance tier of the WebApp and/or the SQL database
3. Disable the SqlRestore function until the primary site comes back online, and you've retrieved any needed data from the Azure DB

# High Availability Configurations
If you would like to have the site automatically fail-over in case of an Umbraco Cloud outage, you can setup additional components to enable this.  These components themselves can incur additional costs, and typically require a higher-tier WebApp to allow them to work.  Review the costs of all components before committing a client to a high-availability configuration.

## Setup the Traffic Manager

1. After ensuring the WebApp is up and running correctly, point DNS of the live site to the WebApp (this is needed to add the hostname and SSL below)
2. Add the hostname and SSL certificate to the WebApp (do not use the SSL only setting)
3. Create a Traffic Manager Profile with a custom name and the Priority routing method
4. On the configuration screen, set the following
    - DNS TTL: 1
    - Protocol: HTTPS
    - Port: 443
    - Path: /
    - Expected Status Code Ranges: 200-299,301-399
    - Probing Interval: 30
    - Tolerated Failures: 3
    - Timeout: 10
5. Add endpoints for the WebApp and your Umbraco Cloud site
    - UC site should have a priority of 1
    - AZ site should have a priority of 2
6. Add the traffic manager URL as a hostname for your Umbraco Cloud site
7. Ensuring your custom traffic manager site is working
8. Repoint DNS to be a CNAME of the traffic manager site

## Make automated nightly DB restore conditional
Make changes to the NightlySqlRestore process so that it doesn't deploy if the backup/{BackupPrefix}{dbName}-pauseAutomaticRestore file exists in the blob storage

## Setup Azure Monitor
Set it up to track Traffic Manager's endpoint status metrics for when the Umbraco Cloud site goes up or down

See blog post at https://blogs.msdn.microsoft.com/waws/2017/12/01/scale-up-appserviceplan-from-azure-function-app/

When it goes down
- Fire a Function to scale up the WebApp from the free tier to a higher tier
- Send a notification to an administrator

When it goes up
- Fire a Function to scale down the WebApp to the free tier
- Send a notification to an administrator
