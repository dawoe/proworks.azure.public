@echo off

setlocal ENABLEDELAYEDEXPANSION

:: ----------------------
:: Umbraco Cloud Config Transfom Script
:: ----------------------

IF NOT DEFINED UAAS_TRANSFORM_PATH (
  SET UAAS_TRANSFORM_PATH="D:\home\site\deployments\tools\transform"
)
IF NOT DEFINED TRANSFORM_DEST (
  SET TRANSFORM_DEST=%1
)
SET UAAS_TRANSFORM_PATH
SET DEPLOYMENT_SOURCE
SET TRANSFORM_DEST

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Config Transformations
:: ----------


set environment=live

echo Handling custom config transforms for %environment%
for /r %DEPLOYMENT_SOURCE% %%a in (*.%environment%.xdt.config) do (
    call :transform %%~dpa %%~nxa %environment%
)

goto:cleanup
EXIT /B

:transform
:: Transform custom config files available in the git repository - naming convention *.development.xdt.config, *.live.xdt.config, etc.
set path=%1
set filename=%2
set sourcefile=%2
set find=.%3.xdt.
set replace=.
call set sourcefile=%%sourcefile:!find!=!replace!%%
set findpath=%DEPLOYMENT_SOURCE%\
set replacepath=\
call set sourcepath=%%path:!findpath!=!replacepath!%%
set finddelim=\
set replacedelim=/
call set sourcepath=%%sourcepath:!finddelim!=!replacedelim!%%

:: In order to get the right destination we have to replace the 'repository' part from 'DEPLOYMENT_SOURCE' (absolute folder path passed in as 1st argument) with 'deployments\___deployTemp'
set destination=%1
IF /I "%DEPLOYMENT_SOURCE%" NEQ "%TRANSFORM_DEST%" (
  set findpart=%DEPLOYMENT_SOURCE%
  set replacepart=%TRANSFORM_DEST%
  call set destination=%%destination:!findpart!=!replacepart!%%
  mkdir %destination% 2>NUL
)

:: In case we need to remove the first part of a transform's file name (to remove the vendor specific part)
call :indexof %sourcefile% %replace% dotindex

IF NOT EXIST %destination%%sourcefile% (
	set sourcefile=!sourcefile:~%dotindex%!
)

echo Transforming %sourcepath%%sourcefile% using %filename%

call %UAAS_TRANSFORM_PATH%\ctt.exe source:%destination%%sourcefile% transform:%path%%filename% destination:%destination%%sourcefile% pw i ic:"  "
IF !ERRORLEVEL! NEQ 0 goto error
goto:end

:indexof [%1 ; %2 ; %3]
@echo off
setlocal enableDelayedExpansion

set "str=%~1"
set "s=!str:%~2=&rem.!"
set s=#%s%
if "%s%" equ "#%~1" endlocal& if "%~3" neq "" (set %~3=-1&exit /b 0) else (echo -1&exit /b 0) 

  set "len=1"
  for %%A in (2187 729 243 81 27 9 3 1) do (
	set /A mod=2*%%A
	for %%Z in (!mod!) do (
		if "!s:~%%Z,1!" neq "" (
			set /a "len+=%%Z"
			set "s=!s:~%%Z!"
			
		) else (
			if "!s:~%%A,1!" neq "" (
				set /a "len+=%%A"
				set "s=!s:~%%A!"
			)
		)
	)
  )
  endlocal & if "%~3" neq "" (set %~3=%len%) else echo %len%
exit /b 0

:cleanup
IF /I "%DEPLOYMENT_SOURCE%" NEQ "%TRANSFORM_DEST%" (
  for /r %DEPLOYMENT_SOURCE% %%a in (*.xdt.config) do (
    del %%~dpnxa
  )
)
echo Finished successfully.
goto:end

:error
echo An error has occurred applying config transforms
call :exitSetErrorLevel
call :exitFromFunction 2>nul

:exitSetErrorLevel
exit /b 1

:exitFromFunction
()

:end
EXIT /B